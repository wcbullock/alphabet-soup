package com.cbullock.AlphabetSoup;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class AlphabetSoup {
    
    private Grid grid;
    private ArrayList<Word> words = null;
    

    /*
     * Constructor creates a BufferedReader to read ASCII text files, since that
     *  is specifically what we are reading. Initializes Grid and Words ArrayList.
     *  
     *  @param String Filename to be read from
     */
    public AlphabetSoup(String fName) throws IOException  {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fName), "US-ASCII"))){
            init(br);
            br.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new IOException(e);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new IOException(e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException(e);
        }
                                
        

    }
    
    /*
     * Initialize AlphabetSoup by creating grid and getting the words to
     * be searched for.
     * 
     * @param BufferedReader initialized to read ASCII text files
     * @return void
     */
    private void init(BufferedReader br) throws IOException 
    {
        try {
            grid = new Grid(br);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException(e);
        }
        setWords(br);
        
    }
    
    /*
     * Populate words ArrayList with the words to be searched for in puzzle.
     * Reads in the lines of config file after the grid is read in. Removes
     * whitespace from words in config file.
     * 
     * @param BufferedReader initialized to read ASCII text files
     * @return void
     * 
     */
    private void setWords(BufferedReader br) throws IOException 
    {
        this.words = new ArrayList<Word>();
        String line;
        
        try {
            while((line = br.readLine()) != null) {
                if(!line.trim().isEmpty())
                    this.words.add(new Word(line.replaceAll(" ",  "")));
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException(e);
        }
        
    }
    
    /*
     * Getter for Word ArrayList
     * 
     * @return ArrayList
     */
    protected ArrayList<Word> getWords()
    {
        return this.words;
    }
    
    /*
     * Find all the words in the puzzle.
     * 
     * @return void
     */
    public void solve()
    {
        for(Word word : words) {
            grid.findWord(word);
        }
    }
    
    /*
     * Print results; traverse Words ArrayList and print coordinates for words.
     * 
     * @return void
     */
    public void printResults()
    {
        for(Word word : words) {
            System.out.println(word.getKey() + " " + word.getCoords());
        }
    }

    /*
     * Main method creates AlphabetSoup, solves, and prints results.
     */
    public static void main(String[] args) throws IOException {
        if(args.length == 0) {
            System.out.println("USAGE: You must supply a filename!");
            System.exit(0);
        }
        String fName = args[0];

        AlphabetSoup alpha = new AlphabetSoup(fName);
        alpha.solve();
        alpha.printResults();
        
    }

}
