package com.cbullock.AlphabetSoup;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

public class Grid {
    
    private int rows = 0;
    private int columns = 0;
    private char[][] grid = null;
    private ArrayList<Word> foundWords = null;
    

    /*
     * Constructor
     * 
     * @param BufferedReader initialized to read ASCII text files
     */
    public Grid(BufferedReader br) throws IOException {
        try {
            init(br);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException(e);
        }
    }
    
    /*
     * Initialize Grid by reading in size and grid from config file.
     * 
     * @param BufferedReader initialized to read ASCII text files.
     * 
     * @return void
     */
    private void init(BufferedReader br) throws IOException  {
        try {
            setSize(br);
            setGrid(br);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException(e);
        }
        foundWords = new ArrayList<Word>();
    }
    
    /*
     * Set the size of the grid by defining number of rows and columns.
     * 
     * @param BufferedReader initialized to read ASCII text files.
     * 
     * @return void
     */
    private void setSize(BufferedReader br) throws IOException  {
        String line;
        try {
            line = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException(e);
        }

        this.rows = Integer.valueOf(line.substring(0, line.indexOf("x")));
        this.columns = Integer.valueOf(line.substring(line.indexOf("x")+1));
        
    }

    /*
     * Read in grid from config file. Uses the amount of rows read in earlier
     * to know how many lines to read in.
     * 
     * @param BufferedReader initialized to read ASCII text files.
     * 
     * @return void
     */
    private void setGrid(BufferedReader br) throws IOException  {
        String line;
        this.grid = new char[this.rows][this.columns];

        for(int i = 0; i< this.rows; i++) {
            try {
                line = br.readLine();
                this.grid[i] = line.replaceAll(" ", "").toCharArray();
            } catch (IOException e) {
                e.printStackTrace();
                throw new IOException(e);
            }
        }
        
    }
    
    /*
     * Find a given word in the Grid. First finds all occurrences of
     * the first character of the word, then searches all directions from each
     * until it finds the entire word. 
     * Sets the beginning position of word in the Word Object, resets it if the
     * word was not found starting from that position.
     * When found, it sets the end position of
     * the word in the Word Object.
     * 
     * @param Word to search for.
     * 
     * @return Word after it was modified to include the end point, or null.
     */
    public Word findWord(Word word)
    {
        String key = word.getKey();
        ArrayList<GridPoint> allFoundStarts;
        GridPoint endPos;
        
        char ch = key.charAt(0);
        allFoundStarts = findAllStarts(ch, word);
        
        for(GridPoint pos : allFoundStarts){
            word.setBegin(pos);
            if((endPos = findRest(word)) != null) {
                word.setEnd(endPos);
                foundWords.add(word);
                return word;
            }
        }
        
        return null;
    }
    
    /*
     * Checks to see if we have already found this word starting at this GridPoint
     * 
     * @param GridPoint
     * @param Word
     * 
     * @return true if we've found it before, false otherwise.
     */
    private boolean found(GridPoint gp, Word word)
    {
    	for(Word wd : foundWords) {
    		if(wd.getKey().equals(word.getKey()))
    		    if(wd.getBegin().isSame(gp))
    			    return true;
    		
    	}
    	
    	return false;
    }
    
    /*
     * Finds the rest of the word starting from an occurrence of the first 
     * character of the word to be found. Searches in 8 (all possible) directions
     * from the character.
     * 
     * @param Word to be searched for.
     * 
     * @return GridPoint containing the endpoint of the word in the grid, or null
     * if not found.
     */
    private GridPoint findRest(Word word)
    {
        GridPoint tmp;
        
        if((tmp = checkLeft(word)) != null) {
            return tmp;
        }else if((tmp = checkRight(word)) != null) {
            return tmp;
        }else if((tmp = checkUp(word)) != null) {
            return tmp;
        }else if((tmp = checkDown(word)) != null) {
            return tmp;
        }else if((tmp = checkLeftDown(word)) != null) {
            return tmp;
        }else if((tmp = checkRightDown(word)) != null) {
            return tmp;
        }else if((tmp = checkRightUp(word)) != null) {
            return tmp;
        }else if((tmp = checkLeftUp(word)) != null) {
            return tmp;
        }
            
        return null;
    }
    
    /*
     * Locates all occurrences of a given character in the grid. Checks to see
     * if we've already found this word at a given point and if so, we don't 
     * include that GridPoint in the ArrayList to be used for the search. This
     * avoids finding the word in the same location again.
     * 
     *@param char to be searched for.
     *@param word to be searched for.
     *
     *@return ArrayList of all occurrences of the char that have not already been
     * used in the beginning of the word.
     */
    private ArrayList<GridPoint> findAllStarts(char ch, Word word)
    {
        GridPoint tmp;
        ArrayList<GridPoint> ret = new ArrayList<GridPoint>();
        
        for(int i=0; i<this.rows; i++) {
            for(int j=0; j<this.columns; j++) {
                if(grid[i][j] == ch) {
                    tmp = new GridPoint(i,j);
                    if(!found(tmp, word))
                        ret.add(tmp);
                }
            }
        }
        
        return ret;
    }
    
    /*
     * Checks to the right of the first character of the word to be found.
     * Short circuits and bails out early if there is not enough room in the
     * grid for the word to potentially exist.
     * 
     * @param Word to be searched for.
     * 
     * @return GridPoint containing the endpoint coordinates of the word, or null.
     */
    private GridPoint checkRight(Word word)
    {
        String wStr = word.getKey();
        int wordLen = wStr.length();
        int cPos = word.getBegin().getCol();
        int numSpaces = columns - cPos;
        int row = word.getBegin().getRow();

        if(wordLen > numSpaces)
            return null;
        
        for(int i = 1; i < wordLen; i++) {
            cPos++;
            if(grid[row][cPos] != wStr.charAt(i))
                return null;
                
        }
        
        return new GridPoint(row, cPos);
    }

    /*
     * Checks to the left of the first character of the word to be found.
     * Short circuits and bails out early if there is not enough room in the
     * grid for the word to potentially exist.
     * 
     * @param Word to be searched for.
     * 
     * @return GridPoint containing the endpoint coordinates of the word, or null.
     */
    private GridPoint checkLeft(Word word)
    {
        String wStr = word.getKey();
        int wordLen = wStr.length();
        int cPos = word.getBegin().getCol();
        int numSpaces = cPos+1;
        int row = word.getBegin().getRow();

        if(wordLen > numSpaces)
            return null;
        
        for(int i = 1; i < wordLen; i++) {
            cPos--;
            if(grid[row][cPos] != wStr.charAt(i))
                return null;
                
        }

        return new GridPoint(row, cPos);
    }

    /*
     * Checks above of the first character of the word to be found.
     * Short circuits and bails out early if there is not enough room in the
     * grid for the word to potentially exist.
     * 
     * @param Word to be searched for.
     * 
     * @return GridPoint containing the endpoint coordinates of the word, or null.
     */
    private GridPoint checkUp(Word word)
    {
        String wStr = word.getKey();
        int wordLen = wStr.length();
        int rPos = word.getBegin().getRow();
        int numSpaces = rPos+1;
        int column = word.getBegin().getCol();

        if(wordLen > numSpaces)
            return null;
        
        for(int i = 1; i < wordLen; i++) {
            rPos--;
            if(grid[rPos][column] != wStr.charAt(i))
                return null;
                
        }

        return new GridPoint(rPos, column);
    }

    /*
     * Checks below of the first character of the word to be found.
     * Short circuits and bails out early if there is not enough room in the
     * grid for the word to potentially exist.
     * 
     * @param Word to be searched for.
     * 
     * @return GridPoint containing the endpoint coordinates of the word, or null.
     */
    private GridPoint checkDown(Word word)
    {
        String wStr = word.getKey();
        int wordLen = wStr.length();
        int rPos = word.getBegin().getRow();
        int numSpaces = rows - rPos;
        int column = word.getBegin().getCol();

        if(wordLen > numSpaces)
            return null;
        
        for(int i = 1; i < wordLen; i++) {
            rPos++;
            if(grid[rPos][column] != wStr.charAt(i))
                return null;
                
        }

        return new GridPoint(rPos, column);
    }

    /*
     * Checks diagonally to the lower left of the first character of the word 
     * to be found.
     * Short circuits and bails out early if there is not enough room in the
     * grid for the word to potentially exist.
     * 
     * @param Word to be searched for.
     * 
     * @return GridPoint containing the endpoint coordinates of the word, or null.
     */
    private GridPoint checkLeftDown(Word word)
    {
        String wStr = word.getKey();
        int wordLen = wStr.length();
        int rPos = word.getBegin().getRow();
        int cPos = word.getBegin().getCol();
        int spacesDown = rows - rPos;
        int spacesLeft = cPos+1;

        if(wordLen > spacesDown || wordLen > spacesLeft)
            return null;
        
        for(int i = 1; i < wordLen; i++) {
            rPos++; cPos--;
            if(grid[rPos][cPos] != wStr.charAt(i))
                return null;
                
        }

        return new GridPoint(rPos, cPos);
    }

    /*
     * Checks diagonally to the upper left of the first character of the word 
     * to be found.
     * Short circuits and bails out early if there is not enough room in the
     * grid for the word to potentially exist.
     * 
     * @param Word to be searched for.
     * 
     * @return GridPoint containing the endpoint coordinates of the word, or null.
     */
    private GridPoint checkLeftUp(Word word)
    {
        String wStr = word.getKey();
        int wordLen = wStr.length();
        int rPos = word.getBegin().getRow();
        int cPos = word.getBegin().getCol();
        int spacesUp = rPos+1;
        int spacesLeft = cPos+1;

        if(wordLen > spacesUp || wordLen > spacesLeft)
            return null;
        
        for(int i = 1; i < wordLen; i++) {
            rPos--; cPos--;
            if(grid[rPos][cPos] != wStr.charAt(i))
                return null;
                
        }

        return new GridPoint(rPos, cPos);
    }

    /*
     * Checks diagonally to the upper right of the first character of the word 
     * to be found.
     * Short circuits and bails out early if there is not enough room in the
     * grid for the word to potentially exist.
     * 
     * @param Word to be searched for.
     * 
     * @return GridPoint containing the endpoint coordinates of the word, or null.
     */
    private GridPoint checkRightUp(Word word)
    {
        String wStr = word.getKey();
        int wordLen = wStr.length();
        int rPos = word.getBegin().getRow();
        int cPos = word.getBegin().getCol();
        int spacesUp = rPos+1;
        int spacesRight = columns - cPos;

        if(wordLen > spacesUp || wordLen > spacesRight)
            return null;
        
        for(int i = 1; i < wordLen; i++) {
            rPos--; cPos++;
            if(grid[rPos][cPos] != wStr.charAt(i))
                return null;
                
        }

        return new GridPoint(rPos, cPos);
    }

    /*
     * Checks diagonally to the lower right of the first character of the word 
     * to be found.
     * Short circuits and bails out early if there is not enough room in the
     * grid for the word to potentially exist.
     * 
     * @param Word to be searched for.
     * 
     * @return GridPoint containing the endpoint coordinates of the word, or null.
     */
    private GridPoint checkRightDown(Word word)
    {
        String wStr = word.getKey();
        int wordLen = wStr.length();
        int rPos = word.getBegin().getRow();
        int cPos = word.getBegin().getCol();
        int spacesDown = rows - rPos;
        int spacesRight = columns - cPos;

        if(wordLen > spacesDown || wordLen > spacesRight)
            return null;
        
        for(int i = 1; i < wordLen; i++) {
            rPos++; cPos++;
            if(grid[rPos][cPos] != wStr.charAt(i))
                return null;
        }

        return new GridPoint(rPos, cPos);
    }


}
