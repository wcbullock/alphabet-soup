package com.cbullock.AlphabetSoup;

public class GridPoint {

    private int row;
    private int column;
    
    /*
     * Constructor
     * 
     * @param int r for row number
     * @param int c for column number
     */
    public GridPoint(int r, int c) 
    {
        this.row = r;
        this.column = c;
    }
    
    /*
     * getter of row
     */
    public int getRow()
    {
        return row;
    }
    
    /*
     * setter of row
     * 
     * @param row number
     */
    public void setRow(int r)
    {
        this.row = r;
    }
    
    /*
     * getter of column
     */
    public int getCol()
    {
        return column;
    }
    
    /*
     * setter of column
     * 
     * @param column number
     */
    public void setCol(int c)
    {
        this.column = c;
    }
    
    /*
     * Compares 2 GridPoints
     * 
     * @param GridPoint
     * @return true if same coordinates, false otherwise.
     */
    public boolean isSame(GridPoint gp)
    {
    	if(gp.getCol() == this.getCol() && gp.getRow() == this.getRow())
    		return true;
    	
    	return false;
    	
    }

}
