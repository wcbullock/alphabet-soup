package com.cbullock.AlphabetSoup;

public class Word {
    
    private String key = null;
    private GridPoint begin = null;
    private GridPoint end = null;

    /*
     * Constructor
     */
    public Word(String val) {
        this.key = val;
    }
    
    /*
     * Return the beginning and endpoint coordinates of the word in the format 
     * the program expects later.
     * 
     * @return String
     */
    public String getCoords()
    {
        return this.getBeginStr() + " " + this.getEndStr();
    }
    
    /*
     * Return Key of Word Object, aka a String representation of the Word.
     * 
     * @return String
     */
    public String getKey()
    {
        return this.key;
    }

    /*
     * Sets the starting point coordinates of the word in the grid.
     * 
     * @param GridPoint
     * @return void
     * 
     */
    public void setBegin(GridPoint begin)
    {
        this.begin = begin;
    }

    /*
     * Sets the endpoint coordinates of the word in the grid.
     * 
     * @param GridPoint
     * @return void
     * 
     */
    public void setEnd(GridPoint end)
    {
        this.end = end;
    }
    
    /*
     * Returns the starting point coordinates of the word in the grid.
     * 
     * @return GridPoint
     * 
     */
    public GridPoint getBegin()
    {
        return this.begin;
    }
    
    /*
     * Returns the endpoint coordinates of the word in the grid.
     * 
     * @return GridPoint
     * 
     */
    public GridPoint getEnd()
    {
        return this.end;
    }

    /*
     * Returns the beginning coordinates of the word in the grid as a String
     * 
     * @return String
     * 
     */
    private String getBeginStr()
    {
        return this.begin.getRow() + ":" + this.begin.getCol();
    }
    
    /*
     * Returns the endpoint coordinates of the word in the grid as a String
     * 
     * @return String
     * 
     */
    private String getEndStr()
    {
        return this.end.getRow() + ":" + this.end.getCol();
    }

}
