package com.cbullock.AlphabetSoup;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AlphabetSoupTest {
    String fName = "src/test/testFiles/testAllDirections";
    AlphabetSoup alpha;

    @Before
    public void setUp() throws Exception {
        alpha = new AlphabetSoup(fName);

    }

    @After
    public void tearDown() throws Exception {
        alpha = null;
    }

    /*
    @Test
    public void testAlphabetSoup() {
        fail("Not yet implemented");
    }
    */

    @Test
    public void testSolve() {
        alpha.solve();
        for(Word word : alpha.getWords())
            assertFalse(word.getEnd() == null);
    }

    /*
    @Test
    public void testMain() {
        fail("Not yet implemented");
    }
    */

}
