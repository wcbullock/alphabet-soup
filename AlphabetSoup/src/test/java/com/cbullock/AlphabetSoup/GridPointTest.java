package com.cbullock.AlphabetSoup;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GridPointTest {
    GridPoint gp;

    @Before
    public void setUp() throws Exception {
        gp = new GridPoint(3,2);
        
    }

    @After
    public void tearDown() throws Exception {
        gp = null;
    }

    /*
    @Test
    public void testGridPoint() {
        fail("Not yet implemented");
    }
    */

    @Test
    public void testGetRow() {
        assertTrue(gp.getRow() == 3);
    }

    @Test
    public void testSetRow() {
        gp.setRow(5);
        assertTrue(gp.getRow() == 5);
    }

    @Test
    public void testGetCol() {
        assertTrue(gp.getCol() == 2);
    }

    @Test
    public void testSetCol() {
        gp.setCol(7);
        assertTrue(gp.getCol() == 7);
    }

    @Test
    public void testIsSame() {
        assertTrue(gp.isSame(gp));
        GridPoint gp2 = new GridPoint(4,5);
        assertFalse(gp.isSame(gp2));
    }

}
