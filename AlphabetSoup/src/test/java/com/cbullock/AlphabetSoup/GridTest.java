package com.cbullock.AlphabetSoup;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GridTest {
    Grid grid;
    String fName = "src/test/testFiles/testAllDirections";
    Word word;
    String wordName = "HELLO";

    @Before
    public void setUp() throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fName), "US-ASCII"));
        grid = new Grid(br);
        br.close();
        word = new Word(wordName);
    }

    @After
    public void tearDown() throws Exception {
        grid = null;
    }

    /*
    @Test
    public void testBoard() {
        fail("Not yet implemented");
    }
    */

    @Test
    public void testFindWord() {
        assertTrue(word.getBegin() == null);
        assertTrue(word.getEnd() == null);
        grid.findWord(word);
        
        assertFalse(word.getBegin() == null);
        assertFalse(word.getEnd() == null);
    }

}
