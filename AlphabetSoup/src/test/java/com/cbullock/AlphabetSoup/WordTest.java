package com.cbullock.AlphabetSoup;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class WordTest {
    Word word;
    String wordName = "testWord";
    String coordStr;
    GridPoint coordTrue;
    GridPoint coordFalse;
    GridPoint begin;
    GridPoint end;

    @Before
    public void setUp() throws Exception {
        this.word = new Word(wordName);
        this.begin = new GridPoint(3,1);
        this.end = new GridPoint(1,3);
        this.coordStr = begin.getRow() + ":" + begin.getCol() + " " + end.getRow() + ":" + end.getCol();
        
        
    }

    @After
    public void tearDown() throws Exception {
        word = null;
    }

    /*
    @Test
    public void testWord() {
        fail("Not yet implemented");
    }
    */

    @Test
    public void testGetCoords() {
        word.setBegin(begin);
        word.setEnd(end);
        assertTrue(word.getCoords().equals(coordStr));
        word.setBegin(null);
        word.setEnd(null);
    }

    @Test
    public void testGetKey() {
        String val = word.getKey();
        assertTrue(val.equals(wordName));
    }

    @Test
    public void testSetBegin() {
        assertTrue(word.getBegin() == null);
        word.setBegin(begin);
        assertTrue(word.getBegin().getRow() ==  begin.getRow());
        assertTrue(word.getBegin().getCol() == begin.getCol());
    }

    @Test
    public void testSetEnd() {
        assertTrue(word.getEnd() == null);
        word.setEnd(end);
        assertTrue(word.getEnd().getRow() == end.getRow());
        assertTrue(word.getEnd().getCol() == end.getCol());
    }

    @Test
    public void testGetBegin() {
        word.setBegin(begin);
        assertTrue(word.getBegin().getRow() == begin.getRow());
        assertTrue(word.getBegin().getCol() == begin.getCol());
    }

    @Test
    public void testGetEnd() {
        word.setEnd(end);
        assertTrue(word.getEnd().getRow() == end.getRow());
        assertTrue(word.getEnd().getCol() == end.getCol());
    }

}
